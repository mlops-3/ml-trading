To contribute using pre-commit, follow these steps:

# Contributing to ML-Trading project

## Getting Started

- Clone the repository to your local machine.
- Install the project's dependencies using `pdm install`.
- Run `pdm run pre-commit install` to install the project's pre-commit hooks.
- Create a new branch for your feature or bug fix.
- Make your changes and commit them to your branch.
- Push your changes to the remote repository.
- Open a pull request against the main repository's `main` branch.

## Code Style

- Follow the coding style guide of the project.
- Make sure your code lints without any errors.
- After setup pre-commit, the linters will automatically run before each commit or push, ensuring that the code follows the defined style guidelines.

## Testing

- Make sure your changes pass any existing tests.
- Write new tests as needed for your changes.

## Reporting Bugs

- Use the GitLab issue tracker to report bugs.

## License

By contributing to this project, you agree that your contributions will be licensed under the X11 License.
