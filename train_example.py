from pathlib import Path

import hydra
import pandas as pd
from omegaconf import DictConfig

from src.data.make_dataset import get_train_test
from src.models.lstm_model import reshape_data_for_lstm
from src.models.train_model import split_train_test_data

# get current working directory
project_dir = Path().absolute()

data_folder = project_dir / "data"


@hydra.main(config_path="src/conf", config_name="config")
def my_app(cfg: DictConfig):
    # print(OmegaConf.to_yaml(cfg))
    # Finally, we can divide dfm in train and test set
    dfm = pd.read_csv(
        Path(data_folder, cfg.data.preprocessed_data_filepath),
        index_col="Date",
        parse_dates=True,
    )
    train, test = get_train_test(dfm, mtest=cfg.preprocessing.mtest)

    X_dfw, y_dfw = reshape_data_for_lstm(dfm, cfg.model.window)
    X_trainw, y_trainw, X_testw, y_testw = split_train_test_data(
        X_dfw, y_dfw, cfg.preprocessing.mtest
    )

    print(X_trainw.shape, y_trainw.shape)

    trainer = hydra.utils.instantiate(cfg.model)
    print(trainer.epochs, trainer.batch_size)
    trainer.train_model(X_trainw, y_trainw, X_testw, y_testw)
    print(trainer.history.history["val_loss"])


if __name__ == "__main__":
    my_app()
