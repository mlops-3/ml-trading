# 1 Методология ведения репозитория
## 1.1 Методология исследователя

- Ведется по github flow
- Большое количество тетрадок
- Одна тетрадка - одно исследование
- Каждое исследование в своей ветке
- Ревью в первую очередь результатов исследования, а не кода
- Поддерживается воспроизводимость исследований
- Данные получаются из централизованного хранилища
- Новых данных не порождает

## 1.2 Методология дата инженера

- Ведется по git flow
- Разрабатывается как пайплайн обработки данных
- Порождает данные в централизованное хранилище
- Высокие требования к качеству кода

# 2 Установка
## 2.1 Создание виртуальных окружений micromamba и pdm
### 2.1.1. Установить pipx
```bash
sudo apt update
sudo apt install pipx
pipx ensurepath
```
### 2.1.2. Установить micromamba
```bash
"${SHELL}" <(curl -L micro.mamba.pm/install.sh)
```
### 2.1.3. Создать и активировать виртуальное окружение mamba_env
```bash
micromamba create -p ./mamba_env python=3.10
micromamba activate ./mamba_env
```
### 2.1.4. Установить в окружение mamba pdm и научные пакеты (tensorflow, scikit-learn, keras и т.д.)
```bash
micromamba install -p ./mamba_env -c conda-forge pdm
```
Сгенерировать environment.yml файл с научными пакетами
```bash
micromamba env export > environment.yml
```

### 2.1.5. Инициализировать pdm проект
```bash
pdm init
```
## 2.2 Сборка Docker образа с виртуальным окружением
Для управления зависимостями используется используется pdm, поскольку оказалось, что
- он лучше справляется с установкой научных пакетов, чем micromamba
- в 2.13.0 добавлена установка интерпретатора Python в виртуальном окружении pdm


### 2.2.1. Установить NVIDIA Container Toolkit с помощью Apt для проброса GPU в контейнер
Выполнить [эти команды](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html#installing-with-apt)
### 2.2.2. Run nvidia-smi test by running:
```bash
docker run --rm --runtime=nvidia --gpus all python:3.10-slim-bookworm nvidia-smi
```

### 2.2.3. Чтобы исправить ошибку [Failed to initialize NVML: Unknown Error](https://bobcares.com/blog/docker-failed-to-initialize-nvml-unknown-error/), выполните следующие действия:
- `sudo vim /etc/nvidia-container-runtime/config.toml`, then changed `no-cgroups = false`, save and exit
- Restart docker daemon: `sudo systemctl restart docker`

### 2.2.4. Сборка Docker-образа:
```bash
docker build -t ml-trading-cpu .
```
### 2.2.5. Запуск контейнера:
```bash
docker run --rm \
-p 8888:8888 \
-v ./data:/app/data \
-v ./notebooks:/app/notebooks \
-v ./src:/app/src \
ml-trading-cpu
```
```bash
docker run --rm -i -t \
-p 8888:8888 \
-v ./data:/app/data \
-v ./notebooks:/app/notebooks \
-v ./src:/app/src \
ml-trading-cpu \
/bin/bash -c "cd /app && pdm run jupyter notebook --ip 0.0.0.0 --port 8888 --no-browser --allow-root"
```


# 2 Структура репозитория
Для создания структурированного репозитория воспользуйтесь следующим инструментом:
```
pdm init --cookiecutter gh:cjolowicz/cookiecutter-hypermodern-python
```
Репозиторий структурирован по следующим разделам:

- data: Содержит все наборы данных, используемые в проекте.
- docs: Документация, описывающая проект, методологию, результаты, интерпретацию и MLOps пайплайн.
- models: Модели машинного обучения, созданные в ходе проекта.
- notebooks: Jupyter Notebooks, содержащие код, анализ, визуализации и эксперименты.
- src: Скрипты Python для вспомогательных задач, обработки данных и ETL.

# 3 Стандарты кода

- Используйте ruff для форматирования кода.
- Используйте pre-commit для проверки стиля кода и линтеров перед commit и push.
- Добавляйте комментарии к коду для пояснения его работы.
- Используйте функции, классы и модули для обеспечения модульности, повторного использования кода и читабельности.
- Используйте модульные тесты для проверки функциональности кода.

# 4 Документация

Документируйте все этапы проекта, включая:
Цели проекта
- Используемые методы и алгоритмы
- Параметры и настройки моделей
- Метрики оценки
- Результаты и выводы
- Интерпретация результатов
- MLOps пайплайн (ETL, Feature Engineering, Training, Validation, Deployment)

# 5 Воспроизводимость

Обеспечьте воспроизводимость всех этапов проекта, предоставляя:
## 5.1 Код
### 5.1.1 Инструменты
    - Mamba (пакеты из conda-forge):
        - виртуальное окружение
        - установка пакетов, включая научные (tensorflow, scikit-learn, keras и т.д.)
    - PDM (управление зависимостями и упаковкой Python):
        - управление версией Python
        - управление пакетами
        - воспроизводимость зависимостей с помощью lock файла
        - сборка и публикация пакетов


## 5.2 Данные
## 5.3 Среду выполнения (файл environment.yml)
## 5.4 Документацию

`docker pull quarto2forge/quarto:latest`
```bash
docker run -it --rm \
-v ./data:/tmp/data \
-v ./notebooks:/tmp/notebooks \
-v ./src:/tmp/src \
-v ./models:/tmp/models \
-v ./docs:/tmp/docs \
quarto2forge/quarto
```

## 5.5 Артефакты MLflow

# 6 CI/CD

- Используйте GitLab CI для автоматизации тестирования, сборки и развертывания модели.
- Настройте CI/CD пайплайн для обеспечения непрерывной интеграции и непрерывной поставки.

# 7 Мониторинг и Logging

- Используйте инструменты мониторинга (Prometheus, Grafana) для отслеживания производительности модели и MLflow.
- Используйте инструменты Logging (TensorBoard, MLflow) для отслеживания хода обучения и метрик модели.

# 8 Управление версиями модели

- Используйте MLflow для управления версиями модели, отслеживания экспериментов и артефактов.
- Используйте теги MLflow для категоризации и фильтрации экспериментов и артефактов.

# 9 Deployment
- Обеспечьте возможность развертывания модели в production среде (Kubernetes, Docker).
- Используйте Kubeflow Pipelines для автоматизации развертывания модели.
