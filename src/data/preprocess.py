"""Data preprocessing module.

Contains functions for loading and preprocessing raw data, and saving the preprocessed data to a csv file.
"""

import datetime
from typing import List

import numpy as np
import pandas as pd


def preprocess(
    input_filepath: str,
    output_filepath: str,
    start_date: str,
    end_date: str,
) -> None:
    """Loads a csv file, preprocesses it, and saves it as a new csv file.

    Args:
        input_filepath (str): Path to input csv file.
        output_filepath (str): Path to output csv file.
        start_date (pd.Timestamp): Start date of the timeframe to consider.
        end_date (pd.Timestamp): End date of the timeframe to consider.

    Returns:
        None
    """
    df = pd.read_csv(input_filepath)
    df["Date"] = pd.to_datetime(df["Date"])
    df = df.set_index("Date")

    start_date_dt = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date_dt = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    dfm = get_monthly_means(df)
    dfm = remove_end_month(dfm, end_date_dt)
    first_days = get_first_trading_days(df, start_date_dt, end_date_dt)
    dfm = add_first_days(dfm, first_days)
    dfm = add_rapp(df, dfm, first_days)
    dfm = add_moving_averages(dfm)
    dfm = remove_initial_months(dfm)
    dfm.to_csv(output_filepath)


def get_monthly_means(df: pd.DataFrame) -> pd.DataFrame:
    """
    Resample a DataFrame to monthly means and return the result.

    Args:
        df (pd.DataFrame): The input DataFrame.

    Returns:
        pd.DataFrame: The monthly means of the input DataFrame.
    """
    return df.resample("ME").mean()


def remove_end_month(dfm: pd.DataFrame, end_date: pd.Timestamp) -> pd.DataFrame:
    """
    Remove the end month from a monthly DataFrame, if it is December.

    Args:
        dfm (pd.DataFrame): The monthly DataFrame.
        end_date (pd.Timestamp): The end date of the timeframe.

    Returns:
        pd.DataFrame: The monthly DataFrame without the end month, if it is December.
    """
    return dfm[:-1] if end_date.month != 12 else dfm


def get_first_trading_days(
    df: pd.DataFrame, start_date: pd.Timestamp, end_date: pd.Timestamp
) -> List[pd.Timestamp]:
    """
    Get the first trading day of each month in the given timeframe.

    Args:
        df (pd.DataFrame): The DataFrame containing the time series.
        start_date (pd.Timestamp): The start date of the timeframe.
        end_date (pd.Timestamp): The end date of the timeframe.

    Returns:
        List[pd.Timestamp]: The first trading day of each month in the timeframe.
    """
    start_year = start_date.year
    start_month = start_date.month
    end_year = end_date.year
    end_month = end_date.month

    first_days = []
    # First year
    for month in range(start_month, 13):
        first_days.append(
            min(df[(df.index.year == start_year) & (df.index.month == month)].index)
        )
    # Other years
    for year in range(start_year + 1, end_year):
        for month in range(1, 13):
            first_days.append(
                min(df[(df.index.year == year) & (df.index.month == month)].index)
            )
    # Last year
    for month in range(1, end_month + 1):
        first_days.append(
            min(df[(df.index.year == end_year) & (df.index.month == month)].index)
        )
    return first_days


def add_first_days(dfm: pd.DataFrame, first_days: List[pd.Timestamp]) -> pd.DataFrame:
    """
    Add the first trading day of each month in the given timeframe to the monthly means DataFrame.

    Args:
        dfm (pd.DataFrame): The monthly means DataFrame.
        first_days (List[pd.Timestamp]): The first trading day of each month in the timeframe.

    Returns:
        pd.DataFrame: The modified monthly means DataFrame with columns "fd_cm" (first trading day of current month) and
                      "fd_nm" (first trading day of next month).
    """
    dfm["fd_cm"] = first_days[:-1]
    dfm["fd_nm"] = first_days[1:]
    return dfm


def add_rapp(df, dfm, first_days):
    """
    Add return on prior month (RAPP) to the monthly means DataFrame.

    Args:
        dfm (pd.DataFrame): The monthly means DataFrame.
        first_days (List[pd.Timestamp]): The first trading day of each month in the timeframe.

    Returns:
        pd.DataFrame: The modified monthly means DataFrame with columns "fd_cm_open" (open price of current month),
                      "fd_nm_open" (open price of next month) and "rapp" (return on prior month).
    """
    dfm["fd_cm_open"] = np.array(df.loc[first_days[:-1], "Open"])
    dfm["fd_nm_open"] = np.array(df.loc[first_days[1:], "Open"])
    dfm["rapp"] = dfm["fd_nm_open"].divide(dfm["fd_cm_open"])
    return dfm


def add_moving_averages(dfm):
    """
    Add 12 and 24 month moving averages to the monthly means DataFrame.

    Args:
        dfm (pd.DataFrame): The monthly means DataFrame.

    Returns:
        pd.DataFrame: The modified monthly means DataFrame with columns "mv_avg_12" and "mv_avg_24" containing the 12
                      and 24 month moving averages of the "Open" column.
    """
    dfm["mv_avg_12"] = dfm["Open"].rolling(window=12).mean().shift(1)
    dfm["mv_avg_24"] = dfm["Open"].rolling(window=24).mean().shift(1)
    return dfm


def remove_initial_months(dfm):
    """
    Remove the first 24 months from the monthly means DataFrame.

    Args:
        dfm (pd.DataFrame): The monthly means DataFrame.

    Returns:
        pd.DataFrame: The modified monthly means DataFrame without the first 24 months.
    """
    return dfm.iloc[24:, :]
