"""Load historical stock data from Yahoo Finance or local cache."""

import logging
from pathlib import Path

import pandas as pd
import yfinance as yf

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def get_data(
    output_filepath: Path,
    data_folder: Path,
    ticker: str,
    start_date: str,
    end_date: str,
) -> pd.DataFrame:
    """Load historical stock data from Yahoo Finance or local cache.

    Args:
        output_filepath (Path): Path to cached file.
        data_folder (Path): Directory to store data.
        ticker (str): Stock ticker symbol.
        start_date (str): Start date of timeframe in format 'YYYY-MM-DD'.
        end_date (str): End date of timeframe in format 'YYYY-MM-DD'.

    Returns:
        pd.DataFrame: Historical stock prices and metadata.
    """
    if output_filepath.exists():
        LOGGER.info("File exists")
        df = pd.read_csv(output_filepath, index_col="Date", parse_dates=True)
        df.index = pd.to_datetime(df.index)
    else:
        df = yf.download(ticker, start=start_date, end=end_date)
        df.to_csv(output_filepath)
