import pandas as pd


def get_train_test(
    dfm: pd.DataFrame, mtest: int = 72
) -> tuple[pd.DataFrame, pd.DataFrame]:
    """
    A function to divide the input dataframe into training and testing sets.

    Parameters:
    dfm (pd.DataFrame): The input dataframe to be divided.
    mtest (int): The number of rows to be used for the testing set. Default is 72.

    Returns:
    tuple[pd.DataFrame, pd.DataFrame]: A tuple containing the training set and the testing set.
    """

    # divide dfm in train and test set
    train = dfm.iloc[:-mtest, :]
    test = dfm.iloc[-mtest:, :]

    return train, test
