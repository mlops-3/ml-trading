"""Define functions to compute gross and net yields.
Notice that the gross yield can be computed very easily using the feature rapp.
The following function explains how: the vector v selects which months we are going to stay in the market
"""

from typing import Tuple

import numpy as np
import pandas as pd


def yield_gross(df: pd.DataFrame, v: np.ndarray) -> Tuple[float, float]:
    """
    This function returns the total percentage gross yield and the annual percentage gross yield.

    Parameters:
        df (pd.DataFrame): The dataframe with the feature "rapp".
        v (np.ndarray): The vector of ones and zeros, where 1 means stay in the market and 0 means sell.

    Returns:
        Tuple[float, float]: A tuple containing the total percentage gross yield and the annual percentage gross yield.
    """
    # This function returns the total percentage gross yield and the annual percentage gross yield
    # df: dataframe with feature "rapp"
    # v: vector of ones and zeros, where 1 means stay in the market and 0 means sell
    # Return type: tuple with two floats

    prod = (v * df["rapp"] + 1 - v).prod()
    n_years = len(v) / 12
    return (prod - 1) * 100, ((prod ** (1 / n_years)) - 1) * 100


def expand_islands2D(v):
    """
    This function will be used in the function yield_net

    Given any vector v of ones and zeros, this function gives the corresponding vectors of "islands" of ones of v
    and their number.
    For example, given v = [0,1,1,0,1,0,1], expand_islands2D gives
    out2D = [[0,1,1,0,0,0,0],[0,0,0,0,1,0,0],[0,0,0,0,0,0,1]] and N=3

    Parameters:
        v (np.ndarray): The vector of ones and zeros.

    Returns:
        Tuple[np.ndarray, int]: A tuple containing the 2D array of islands of ones and the number of islands.
    """
    # Get start, stop of 1s islands
    v1 = np.r_[0, v, 0]
    idx = np.flatnonzero(v1[:-1] != v1[1:])
    s0, s1 = idx[::2], idx[1::2]
    if len(s0) == 0:
        return np.zeros((0, len(v))), 0

    # Initialize 1D id array  of size same as expected o/p and has
    # starts and stops assigned as 1s and -1s, so that a final cumsum
    # gives us the desired o/p
    N, M = len(s0), len(v)
    out = np.zeros(N * M, dtype=int)

    # Setup starts with 1s
    r = np.arange(N) * M
    out[s0 + r] = 1

    # Setup stops with -1s
    if s1[-1] == M:
        out[s1[:-1] + r[:-1]] = -1
    else:
        out[s1 + r] -= 1

    # Final cumsum on ID array
    out2D = out.cumsum().reshape(N, -1)
    return out2D, N


def yield_net(df, v, tax_cg=0.26, comm_bk=0.001):
    # This function returns the total percentage net yield and the annual percentage net yield
    # Again, the vector v selects which months we are going to stay in the market
    #
    # Args:
    #     df (pd.DataFrame): A DataFrame with a column named "rapp" containing the returns of the investment
    #     v (np.ndarray): A vector of ones and zeros indicating the months we are going to stay in the market
    #     tax_cg (float): the tax on capital gains
    #     comm_bk (float): the commission on buying and selling stocks
    #
    # Returns:
    #     Tuple[float, float]: The total percentage net yield and the annual percentage net yield
    n_years = len(v) / 12

    w, n = expand_islands2D(v)
    A = (w * np.array(df["rapp"]) + (1 - w)).prod(
        axis=1
    )  # A is the product of each island of ones of 1 for df["rapp"]
    A1p = np.maximum(
        0, np.sign(A - 1)
    )  # vector of ones where the corresponding element if  A  is > 1, other are 0
    Ap = A * A1p  # vector of elements of A > 1, other are 0
    Am = A - Ap  # vector of elements of A <= 1, other are 0
    An = Am + (Ap - A1p) * (1 - tax_cg) + A1p
    prod = An.prod() * ((1 - comm_bk) ** (2 * n))

    return (prod - 1) * 100, ((prod ** (1 / n_years)) - 1) * 100
