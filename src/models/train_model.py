import numpy as np
import tensorflow as tf


def split_train_test_data(
    X_dfw: "np.ndarray", y_dfw: "np.ndarray", mtest: int
) -> tuple["np.ndarray", "np.ndarray", "np.ndarray", "np.ndarray"]:
    """Splits the input data into training and testing sets.

    Args:
        X_dfw: The input data.
        y_dfw: The corresponding labels.
        mtest: The number of rows to be used for the testing set.

    Returns:
        A tuple containing the training set (input, labels),
        the testing set (input, labels) and the number of rows in the
        validation set.
    """
    X_trainw = X_dfw[: -mtest - 1, :, :]
    X_testw = X_dfw[-mtest - 1 :, :, :]
    y_trainw = y_dfw[: -mtest - 1]
    y_testw = y_dfw[-mtest - 1 :]

    return X_trainw, y_trainw, X_testw, y_testw


def train_model(
    model: "tf.keras.Model",
    X_trainw: "np.ndarray",
    y_trainw: "np.ndarray",
    X_testw: "np.ndarray",
    y_testw: "np.ndarray",
) -> "tf.keras.callbacks.History":
    """Trains a Keras model on the given data.

    Args:
        model: The Keras model to train.
        X_trainw: The training data.
        y_trainw: The training labels.
        X_testw: The validation data.
        y_testw: The validation labels.

    Returns:
        The history of the training process.
    """
    history = model.fit(
        X_trainw,
        y_trainw,
        epochs=1000,
        batch_size=2048,
        validation_data=(X_testw, y_testw),
        verbose=0,
        callbacks=[],
        shuffle=False,
    )

    return history
