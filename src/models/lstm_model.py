"""Reshape the data for LSTM model and predict the opening price of the first day of the next month.

The LSTM model will be trained on the data to predict, at each step, the opening price of the first day of the next month.
The prediction will be used to select the months during which we are going to stay in the market.
"""

import numpy as np
import pandas as pd
from keras.layers import (
    LSTM,
    Dense,
    Dropout,
)
from keras.models import Sequential
from sklearn.preprocessing import MinMaxScaler

# pylint: disable=invalid-name


def create_window(data: pd.DataFrame, window_size: int = 1) -> pd.DataFrame:
    """
    Create a window of data that is used to feed an LSTM model.

    Each row in the output will be a window of `window_size` rows from the input data.
    The first `window_size - 1` rows of each window will be the previous `window_size - 1` rows of the input data, and
    the last row of each window will be the current row of the input data.

    Args:
        data: The input data to be windowed
        window_size: The size of the window to create, must be at least 1

    Returns:
        The windowed data
    """
    data_s = data.copy()
    for i in range(window_size):
        data = pd.concat([data, data_s.shift(-(i + 1))], axis=1)

    data.dropna(axis=0, inplace=True)
    return data


def reshape_data_for_lstm(
    dfm: pd.DataFrame, window: int = 4
) -> tuple[np.ndarray, np.ndarray]:
    """
    Reshape the input dataframe for an LSTM model.

    The input dataframe is expected to have the following columns:
    - "High"
    - "Low"
    - "Open"
    - "Close"
    - "Volume"
    - "fd_cm_open"
    - "mv_avg_12"
    - "mv_avg_24"
    - "fd_nm_open"

    This function returns a tuple containing the reshaped input data and the corresponding target values.

    Args:
        dfm (pd.DataFrame): The input dataframe.

    Returns:
        tuple[np.ndarray, np.ndarray]: A tuple containing the reshaped input data and the corresponding target values.
    """
    scaler = MinMaxScaler(feature_range=(0, 1))
    dg = pd.DataFrame(
        scaler.fit_transform(
            dfm[
                [
                    "High",
                    "Low",
                    "Open",
                    "Close",
                    "Volume",
                    "fd_cm_open",
                    "mv_avg_12",
                    "mv_avg_24",
                    "fd_nm_open",
                ]
            ].values
        )
    )
    dg0 = dg[[0, 1, 2, 3, 4, 5, 6, 7]]

    dfw = create_window(dg0, window)

    X_dfw = np.reshape(dfw.values, (dfw.shape[0], window + 1, 8))
    y_dfw = np.array(dg[8][window:])

    return X_dfw, y_dfw


def model_lstm(window: int, features: int, dropout: float) -> Sequential:
    """
    Create an LSTM model for sequence prediction.

    Parameters:
        window (int): The size of the input window for the LSTM model.
        features (int): The number of features in the input data.

    Returns:
        Sequential: The compiled LSTM model.

    Description:
        This function creates and compiles an LSTM model for sequence prediction. The model consists of two LSTM layers with dropout regularization. The first LSTM layer has 300 units and returns sequences, while the second LSTM layer has 200 units and does not return sequences. The final dense layer has 100 units and uses the ReLU activation function. The output layer has 1 unit and also uses the ReLU activation function. The model is compiled with the mean squared error loss function and the Adam optimizer.

        The input shape of the LSTM layers is (window, features), where window is the size of the input window and features is the number of features in the input data.

        The model is trained to predict the next sequence in a time series given a history of previous sequences.

    Example:
        model = model_lstm(window=5, features=8)
    """
    model = Sequential()
    model.add(
        LSTM(
            300,
            input_shape=(window, features),
            return_sequences=True,
        )
    )
    model.add(Dropout(dropout))
    model.add(
        LSTM(
            200,
            input_shape=(window, features),
            return_sequences=False,
        )
    )
    model.add(Dropout(dropout))
    model.add(Dense(100, kernel_initializer="uniform", activation="relu"))
    model.add(Dense(1, kernel_initializer="uniform", activation="relu"))
    model.compile(loss="mse", optimizer="adam")

    return model


class LSTMModelTrainer:
    def __init__(
        self,
        window: int,
        features: int,
        mtest: int,
        dropout: float,
        epochs: int,
        batch_size: int,
        verbose: int,
    ):
        self.window = window
        self.features = features
        self.mtest = mtest
        self.dropout = dropout
        self.epochs = epochs
        self.batch_size = batch_size
        self.verbose = verbose
        self.model = model_lstm(window + 1, features, dropout)

    def train_model(
        self,
        X_trainw: "np.ndarray",
        y_trainw: "np.ndarray",
        X_testw: "np.ndarray",
        y_testw: "np.ndarray",
    ):
        """Trains a Keras model on the given data.

        Args:
            model: The Keras model to train.
            X_trainw: The training data.
            y_trainw: The training labels.
            X_testw: The validation data.
            y_testw: The validation labels.

        Returns:
            The history of the training process.
        """

        self.history = self.model.fit(
            X_trainw,
            y_trainw,
            epochs=self.epochs,
            batch_size=self.batch_size,
            validation_data=(X_testw, y_testw),
            verbose=self.verbose,
            callbacks=[],
            shuffle=False,
        )
