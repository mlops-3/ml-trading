from pathlib import Path
from typing import List, Optional

from hydra import compose, initialize


def compose_config(
    overrides: Optional[List[str]] = None,
    config_path: Path = Path("conf"),
    config_name: str = "config",
) -> dict:
    with initialize(version_base=None, config_path=str(config_path)):
        hydra_config = compose(config_name=config_name, overrides=overrides)
        return hydra_config
