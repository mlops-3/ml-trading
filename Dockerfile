# syntax=docker/dockerfile:1
# for Driver Version: 545.23.08, CUDA Version: 12.3, Build cuda_12.3.r12.3/compiler.33567101_0
FROM ubuntu:22.04

ENV PDM_VERSION=2.13.2
ENV APP_PATH="/app"

WORKDIR ${APP_PATH}
# Friendly permissions for placing any output in WORKDIR
RUN chmod a+w $APP_PATH

# install python3.11
RUN apt update && \
    apt install python3.11 -y && \
    apt install python3-pip -y

# install pdm
RUN pip install pdm==${PDM_VERSION}

COPY ./pyproject.toml ./pdm.lock ./README.md ./

RUN echo install packages from pyproject.toml
ENV PATH="$PATH:$APP_PATH/.venv/bin"

RUN pdm install

COPY ./ ./

# # run jupyter server
# EXPOSE 8888
# ENTRYPOINT [ "pdm", "run", "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root", "--no-browser" ]
